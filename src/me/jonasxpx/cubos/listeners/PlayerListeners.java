package me.jonasxpx.cubos.listeners;

import java.text.NumberFormat;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.EnderDragon;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import me.jonasxpx.cubos.Cubos;
import me.jonasxpx.cubos.conquistas.Conquista;
import me.jonasxpx.cubos.conquistas.Level;
import me.jonasxpx.cubos.conquistas.events.PlayerLevelUpEvent;
import me.jonasxpx.cubos.inventarios.Conquistas;
import me.jonasxpx.cubos.inventarios.loja.ItemCmd;
import me.jonasxpx.cubos.tools.Economy;
import me.jonasxpx.cubos.tools.Premios.Reward;
import me.jonasxpx.cubos.tools.Premios.TypePremios;
import me.jonasxpx.cubos.tools.Sounds;
import me.jonasxpx.cubos.user.AdminMode;
import me.jonasxpx.cubos.user.Transferencia;
import me.jonasxpx.cubos.user.User;
import me.jonasxpx.killranking.event.PlayerKillEvent;

public class PlayerListeners implements Listener{

	@EventHandler
	public void onJoinPlayer(PlayerJoinEvent e) {
		User user = new User(e.getPlayer().getName());
		user.load();
		Cubos.getUsersList().add(user);
	}
	
	@EventHandler
	public void onPlayerClick(InventoryClickEvent e) {
		if(!e.getInventory().getName().startsWith("iCubos |")) {
			if(e.getInventory().getName().startsWith("Loja iCubos")) {
				lojaClickManager(e);
			}
			return;
		}
		e.setCancelled(true);
		ItemStack item = e.getCurrentItem();
		Player player = (Player)e.getWhoClicked();
		Conquistas c = new Conquistas(player.getName(), false);
		
		if(item == null)
			return;
		
		if(item.hasItemMeta() && (item.getItemMeta().getDisplayName().contains("Voc� n�o tem") || item.getItemMeta().getDisplayName().contains("Adquirir"))) {
			c.openInventory(true);
			return;
		}
		if(item.getType() == Material.ENCHANTED_BOOK) {
			String name = item.hasItemMeta() ? item.getItemMeta().getDisplayName().substring(2) : "";
			Conquista cq = Cubos.getConquistasByName(name);
			User user = Cubos.getUser(player.getName());
			if(Economy.isAllowedToBuy(cq.getValor(), player)) {
				user.getPlayerConquistas().addConquista(cq);
				player.sendMessage("�dConquista Adquirida!");
				c.openInventory(false);
				Sounds.SUCCESS.playSound(player);
			} else {
				player.sendMessage("�cVoc� n�o tem dinheiro suficiente para comprar.");
				Sounds.ERROR.playSound(player, 2, 1);
			}
			player.closeInventory();
			return;
		}
		
	}
	
	private void lojaClickManager(InventoryClickEvent e) {
		e.setCancelled(true);
		ItemCmd i = ItemCmd.getItemByItemStack(e.getCurrentItem());
		if(i == null) {
			return;
		}
		Player player = (Player)e.getWhoClicked(); User user = Cubos.getUser(player.getName());
		if(AdminMode.getTotalCubos(user) < i.getPrice()) {
			player.sendMessage(Cubos.TAG + " Voc� n�o tem Cubos suficiente.");
			return;
		}
		try {
			if(i.getClassOut() != null) {
				i.getClassOut().method((Player)e.getWhoClicked());
				if(i.getClassOut().isCanceled()) {
					return;
				} else {
					AdminMode.takeICubos(user, i.getPrice());
					Cubos.log(java.util.logging.Level.INFO, "@class takeiCubos from: " + user.getUser() + " amount: " + i.getPrice());
				}
			} else {
				AdminMode.takeICubos(user, i.getPrice());
				Cubos.log(java.util.logging.Level.INFO, "takeiCubos from:" + user.getUser() + " amount: " + i.getPrice());
			}
			if(i.getCommandOut().size() > 0) {
				i.getCommandOut().forEach(cmd -> {
					Bukkit.dispatchCommand(Cubos.getCubosInstance().getServer().getConsoleSender(), cmd
							.replaceAll("@player", player.getName()));
				});
			}
		} catch (IllegalArgumentException ex) {
			player.sendMessage(Cubos.TAG + " Voc� n�o tem Cubos suficiente."); Cubos.getCubosInstance().getLogger().log(java.util.logging.Level.WARNING, ex.getMessage());
		} catch (Exception e2) {
			e2.printStackTrace();Cubos.getCubosInstance().getLogger().log(java.util.logging.Level.WARNING, e2.getMessage());
		} finally {
			player.closeInventory();
		}
	}
	
	@EventHandler
	public void onLeavePlayer(PlayerQuitEvent e) {
		User toRemove = null;
		for(User user : Cubos.getUsersList())
			if(user.getUser() == e.getPlayer().getName()) {
				toRemove = user;
				break;
			}
		if(toRemove != null) {
			toRemove.saveData();
			Cubos.getUsersList().remove(toRemove);
		}
	}
	
	
	
	/*
	 * Para controle de transferencias
	 * Entre jogadores
	 */
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		if(Cubos.getTransferencias().containsKey(e.getPlayer().getName())) {
			Transferencia t = Cubos.getTransferencias().get(e.getPlayer().getName());
			if(e.getMessage().equalsIgnoreCase("sim"))
				t.confirmar();
			else if(e.getMessage().equalsIgnoreCase("n�o") || e.getMessage().equalsIgnoreCase("nao"))
				t.cancelar();
			else
				t.naoEfetuada();
		}
	}
	
	
	/*
	 * Aqui as conquistas de Porcos � escutada,
	 * quando o jogador mata um porco.
	 */
	@EventHandler
	public void onPlayerDamage(EntityDeathEvent e) {
		if(e.getEntity().getKiller() == null)
			return;
		
		User user = Cubos.getUser(e.getEntity().getKiller().getName());
		if(e.getEntity() instanceof Pig) {
			Conquista conquista;
			if((conquista = user.getPlayerConquistas().getConquista("porcos")) != null) {
				Level l = user.getPlayerConquistas().listenerConquistas.get(conquista);
				l.giveExperience(conquista.getExp());
			}
		}
		if(e.getEntity() instanceof EnderDragon) {
			Conquista conquista;
			if((conquista = user.getPlayerConquistas().getConquista("dracokiller")) != null) {
				Level l = user.getPlayerConquistas().listenerConquistas.get(conquista);
				l.giveExperience(conquista.getExp());
			}
		}
	}
	
	@EventHandler
	public void onPlayerKill(PlayerKillEvent e) {
		if(e.getPlayer().getKiller() == null) {
			return;
		}
		Player player = e.getPlayer().getKiller();
		User user = Cubos.getUser(player.getName());
		Conquista conquista;
		if((conquista = user.getPlayerConquistas().getConquista("matan�a")) != null) {
			Level l = user.getPlayerConquistas().listenerConquistas.get(conquista);
			l.giveExperience(conquista.getExp());
		}
	}
	
	
	@EventHandler
	public void onPlayerLevelUp(PlayerLevelUpEvent event) {
		event.getPlayer().sendMessage("�6["+event.getConquista().getName() + "] �bVoc� subiu de n�vel: �a" + event.getLevel().getLevel() + " �bNovo objetivo de XP: �a" + event.getLevel().getMaxExperience());
		Bukkit.broadcastMessage("�7[�2iCubos�7] �b�o"+event.getPlayer().getName() + "�b subiu de n�vel em �o" + event.getConquista().getName());
		User user = Cubos.getUser(event.getPlayer().getName());
		Reward[] rw = event.getConquista().getPremiacao().getReward(event.getLevel().getLevel());
		for(Reward r : rw) {
			if(r.getType() == TypePremios.CUBOS) {
				AdminMode.giveICubos(user, r.getValor());
				event.getPlayer().sendMessage("�2Voc� recebeu �f" + r.getValor() + " Cubo(s).");
			} else if (r.getType() == TypePremios.MONEY) {
				Cubos.getEconomy().depositPlayer(event.getPlayer(), r.getValor());
				event.getPlayer().sendMessage("�2Voc� recebeu �f" + NumberFormat.getInstance().format(r.getValor()) + " Coins.");
			}
		}
		
	}
}