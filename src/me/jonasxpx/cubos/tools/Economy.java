package me.jonasxpx.cubos.tools;

import org.bukkit.entity.Player;

import me.jonasxpx.cubos.Cubos;

public class Economy {

	
	public static boolean isAllowedToBuy(double amount, Player player) {
		boolean allow = Cubos.getEconomy().getBalance(player) >= amount;
		if(allow)
			Cubos.getEconomy().withdrawPlayer(player, amount);
		return allow;
	}
	
	
}
