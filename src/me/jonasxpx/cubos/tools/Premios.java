package me.jonasxpx.cubos.tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Premios {

	
	private HashMap<Level, Reward> levels;
	
	
	public Premios() {
		levels = new HashMap<>();
	}
	
	
	public void setLevels(Level level) {
		levels.put(level, new Reward(0, null));
	}
	
	public void setLevels(Level level, int reward, TypePremios rewardType) {
		levels.put(level, new Reward(reward, rewardType));
	}
	
	
	public Reward[] getReward(int level) {
		List<Reward> rewards = new ArrayList<>();
		for(Level l : levels.keySet()) {
			if(l.isBetween(level)) {
				rewards.add(levels.get(l));
			}
		}
		return rewards.toArray(new Reward[rewards.size()]);
	}
	
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Level l : levels.keySet()) {
			sb.append(l.toString());
			sb.append(levels.get(l));
			sb.append(" <-> ");
		}
		sb.append("Total: ");
		sb.append(levels.size());
		return sb.toString();
	}
	
	
	public class Reward {
		
		private int valor;
		private TypePremios type;
		
		public Reward(int valor, TypePremios type) {
			this.valor = valor;
			this.type = type;
		}
		
		public TypePremios getType() {
			return type;
		}
		public int getValor() {
			return valor;
		}
		
	}
	
	
	public enum TypePremios{
		CUBOS("icubos give {player} {amount}", "Cubos"),
		MONEY("money give {player} {amount}", "Money");

		private String cmd;
		private String name;
		
		private TypePremios(String cmd, String name) {
			this.cmd = cmd;
			this.name= name;
		}
		
		public String getCmd() {
			return cmd;
		}
		public String getName() {
			return name;
		}
	}
	
}
