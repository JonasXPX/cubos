package me.jonasxpx.cubos.tools;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

import com.google.common.collect.Lists;

import me.jonasxpx.cubos.Cubos;
import me.jonasxpx.cubos.inventarios.loja.CommandOut;

public class ItemConfigRead {

	private static String ms;
	private final FileConfiguration config;
	private static String SELECTION;
	
	public ItemConfigRead(String memorySelection, FileConfiguration file) {
		this.ms = memorySelection;
		this.config = file;
		SELECTION = "itens." + ms + ".";
	}
	
	public int getItemId() {
		if(!config.contains(SELECTION + "itemid")) {
			config.set(SELECTION + "itemid", 57);
			update();
		}
		return config.getInt(SELECTION + "itemid");
	}
	
	public String name() {
		if(!config.contains(SELECTION + "name")) {
			config.set(SELECTION + "name", "");
			update();
		}
		return ChatColor.translateAlternateColorCodes('&', config.getString(SELECTION + "name"));
	}
	
	public List<String> label(){
		if(!config.contains(SELECTION + "descricao")) {
			config.set(SELECTION + "descricao", Arrays.asList(""));
			update();
		}
		List<String> label = config.getStringList(SELECTION + "descricao");
		List<String> tLabel = Lists.newArrayList();
		label.forEach(p -> tLabel.add(ChatColor.translateAlternateColorCodes('&', p)));
		return tLabel;
	}
	
	public List<String> getCommandOut() {
		if(!config.contains(SELECTION + "commands")) {
			config.set(SELECTION + "commands", Arrays.asList(""));
			update();
		}
		return config.getStringList(SELECTION + "commands");
	}
	
	public int getPrice() {
		if(!config.contains(SELECTION + "price")) {
			config.set(SELECTION + "price", 0);
			update();
		}
		return config.getInt(SELECTION + "price");
	}
	
	public CommandOut getClassOut() throws MalformedURLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
		if(!config.contains(SELECTION + "class")) {
			return null;
		}
		File fclass = new File(Cubos.getCubosInstance().getDataFolder() + File.separator + "classes" + File.separator + config.getString(SELECTION + "class") + ".class");
		if(!fclass.exists()) {
			Cubos.getCubosInstance().getLogger().log(Level.SEVERE, "Classe " + fclass.getAbsolutePath() + " n�o encontrada!.");
			return null;
		}
		
		File folder = new File(Cubos.getCubosInstance().getDataFolder() + File.separator + "classes");
		String name = config.getString(SELECTION + "class"); 
		ClassLoader cl = new URLClassLoader(new URL[]{folder.toURI().toURL()}, CommandOut.class.getClassLoader());
		Class<?> clazz = cl.loadClass(name);
		Object object = clazz.newInstance();
		if(!(object instanceof CommandOut)) {
			System.out.println("Erro classe sem instancia CommandOut");
			return null;
		}
		CommandOut cm = (CommandOut)object;
		return cm;
		
	}
	
	private void update() {
		Cubos.getCubosInstance().saveConfig();
	}
}
