package me.jonasxpx.cubos.tools;

import org.bukkit.Sound;
import org.bukkit.entity.Player;;

public enum Sounds {

	SUCCESS(Sound.ORB_PICKUP),
	ERROR(Sound.ANVIL_LAND);

	private Sound sound;
	
	private Sounds(Sound s) {
		this.sound = s;
	}
	
	
	public void playSound(Player player) {
		player.playSound(player.getLocation(), this.sound, 1, 1);
	}
	public void playSound(Player player, float pitch, float volume) {
		player.playSound(player.getLocation(), this.sound, volume, pitch);
	}
}
