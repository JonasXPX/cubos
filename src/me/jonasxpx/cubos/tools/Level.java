package me.jonasxpx.cubos.tools;

public class Level {

	private int startLevel;
	private int endLevel;
	
	public Level(int startLevel, int endLevel) {
		this.startLevel = startLevel;
		this.endLevel = endLevel;
	}

	public boolean isBetween(int level) {
		if(level > startLevel && level <= endLevel) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Start: ");
		sb.append(startLevel);
		sb.append(" -> End: ");
		sb.append(endLevel);
		sb.append(" = ");
		return sb.toString();
	}
}
