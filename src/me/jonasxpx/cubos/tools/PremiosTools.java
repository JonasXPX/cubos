package me.jonasxpx.cubos.tools;

import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import me.jonasxpx.cubos.tools.Premios.TypePremios;

public class PremiosTools {

	
	public static Premios executePremios(JSONObject l) {
		Premios premios = new Premios();
		try {
			if(l.has("cubos")) {
				JSONObject cubos = l.getJSONObject("cubos");
				Iterator<Object> i = cubos.keys();
				i.forEachRemaining(p -> {
					String[] lv = ((String)p).split("-");
					Level level = new Level(Integer.parseInt(lv[0]), Integer.parseInt(lv[1]));

					try {
						premios.setLevels(level, cubos.getInt((String)p), TypePremios.CUBOS);
					} catch (JSONException e) {e.printStackTrace();}
				});
			}
			if(l.has("money")) {
				JSONObject cubos = l.getJSONObject("money");
				Iterator<Object> i = cubos.keys();
				i.forEachRemaining(p -> {
					String[] lv = ((String)p).split("-");
					Level level = new Level(Integer.parseInt(lv[0]), Integer.parseInt(lv[1]));
					try {
						premios.setLevels(level, cubos.getInt((String)p), TypePremios.MONEY);
					} catch (JSONException e) {e.printStackTrace();}
				});
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return premios;
	}
}
