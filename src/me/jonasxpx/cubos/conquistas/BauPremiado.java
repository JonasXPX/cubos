package me.jonasxpx.cubos.conquistas;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BauPremiado {

	private Map<Integer, ItemStack[]> itens = new HashMap<>();
	
	public BauPremiado() {
		
	}
	
	public ItemStack[] getItens(int id) {
		return itens.get(id);
	}
	
	public void setItens(ItemStack[] item, int id) {
		itens.put(id, item);
	}
	
	
	public JSONObject toJson() {
		JSONObject jsItens = new JSONObject();
		for(Integer i : itens.keySet()) {
			ItemStack[] s = itens.get(i);
			JSONArray jsonItens = new JSONArray();
			for(ItemStack in : s) {
				int amount = in.getAmount();
				String type = in.getType().name();
				Map<String, Integer> enchants = new HashMap<>();
				List<String> lore = null;
				if(in.hasItemMeta()) {
					ItemMeta meta = in.getItemMeta();
					if(meta.hasEnchants()) {
						meta.getEnchants().forEach((e,l) -> {
							enchants.put(e.getName(), l);
						});
					}
					lore = meta.getLore();
				}
				JSONObject js = new JSONObject();
				try {
					js.put("amount", amount);
					js.put("type", type);
					js.put("enchants", enchants);
					js.put("lore", lore);
				}catch (Exception e) {
					e.printStackTrace();
				}
				jsonItens.put(js);
			}
			try {
				jsItens.put(i.toString(), jsonItens);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return jsItens;
	}
	
}
