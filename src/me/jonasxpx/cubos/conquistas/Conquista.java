package me.jonasxpx.cubos.conquistas;

import org.bukkit.entity.Player;
import org.json.JSONException;
import org.json.JSONObject;

import me.jonasxpx.cubos.Cubos;
import me.jonasxpx.cubos.conquistas.events.PlayerLevelUpEvent;
import me.jonasxpx.cubos.tools.Premios;

public class Conquista {

	private String name;
	private double razao;
	private int id;
	private double exp;
	private double valor;
	private String descricao;
	private Premios premiacao;
	
	public Conquista(String name, double razao, int id, Premios premiacao) {
		this.name = name;
		this.razao = razao;
		this.id = id;
		this.premiacao = premiacao;
	}
	
	public void callLevelUp(Level level, Player player) {
		PlayerLevelUpEvent up = new PlayerLevelUpEvent(player, level, this);
		Cubos.getCubosInstance().getServer().getPluginManager().callEvent(up);
	}

	public Premios getPremiacao() {
		return premiacao;
	}
	
	public void setPremiacao(Premios premiacao) {
		this.premiacao = premiacao;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getRazao() {
		return razao;
	}

	public void setRazao(double razao) {
		this.razao = razao;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public double getExp() {
		return exp;
	}
	
	public void setExp(double exp) {
		this.exp = exp;
	}
	
	public double getValor() {
		return valor;
	}
	
	public void setValor(double valor) {
		this.valor = valor;
	}
	
	@Override
	public String toString() {
		JSONObject jo = new JSONObject();
		try {
			jo.put("valor", getValor());
			jo.put("exp", getExp());
			jo.put("id", getId());
			jo.put("razao", getRazao());
			jo.put("name", getName());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return jo.toString();
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
