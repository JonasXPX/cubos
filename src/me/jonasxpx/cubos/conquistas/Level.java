package me.jonasxpx.cubos.conquistas;

import org.bukkit.entity.Player;

public class Level{

	public double xp = 0;
	public int level = 1;
	private double RAZAO;
	private Conquista conquista;
	private Player player;
	
	public Level(Conquista conquista, int level, double xp, Player player) {
		this.xp = xp;
		this.level = level;
		this.RAZAO = conquista.getRazao();
		this.conquista = conquista;
		this.player = player;
	}
	
	public void giveExperience(double ex) {
		this.xp+=ex;
		if(xp > (RAZAO * level)) {
			level++;
			xp = 0;
			conquista.callLevelUp(this, player);
		}
	}
	
	public double getExperience() {
		return this.xp;
	}
	
	public int getMaxExperience() {
		return (int)RAZAO * level;
	}
	
	public int getLevel() {
		return this.level;
	}
	
}