package me.jonasxpx.cubos.conquistas.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.jonasxpx.cubos.conquistas.Conquista;
import me.jonasxpx.cubos.conquistas.Level;

public class PlayerLevelUpEvent extends Event{

	private static final HandlerList handlers = new HandlerList();
	private final Player player;
	private final Level level;
	private final Conquista conquista;
	
	public PlayerLevelUpEvent(Player player, Level level, Conquista conquista) {
		this.player = player;
		this.level = level;
		this.conquista = conquista;
	}
	
	
	public Player getPlayer() {
		return player;
	}
	
	public Level getLevel() {
		return level;
	}
	
	public Conquista getConquista() {
		return conquista;
	}
	
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}

	
}
