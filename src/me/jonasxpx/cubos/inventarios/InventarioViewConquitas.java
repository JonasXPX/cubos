package me.jonasxpx.cubos.inventarios;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.jonasxpx.cubos.Cubos;
import me.jonasxpx.cubos.conquistas.Conquista;
import me.jonasxpx.cubos.conquistas.Level;
import me.jonasxpx.cubos.tools.Premios.Reward;
import me.jonasxpx.cubos.user.User;

public class InventarioViewConquitas {

	private final String player;
	private final User user;
	private final String view;
	
	public InventarioViewConquitas(String player, String view) {
		this.player = player;
		this.user = Cubos.getUser(player);
		this.view = view;
	}
	
	public Inventory openInventory() {
		Inventory inventory = Bukkit.createInventory(null, 54, "iCubos | Conquistas");
		for(Conquista c : user.getPlayerConquistas().listenerConquistas.keySet()) {
			Level level = user.getPlayerConquistas().listenerConquistas.get(c);
			ItemStack item = new ItemStack(level.getLevel() > 50 ? Material.ENCHANTED_BOOK : Material.BOOK);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName("�b" + StringUtils.capitalize(c.getName()));
			List<String> lore = new ArrayList<>();
			lore.add("�aLevel: �6" + level.getLevel());
			lore.add("�aExperiencia: �6" + (int)level.getExperience() + "/" + level.getMaxExperience());
			lore.add("");
			lore.add("�6Informa��es sobre a conquista:");
			lore.add("�bExperiencia a ganhar: �a" + c.getExp());
			lore.add("�bRazao de dificuldade: �a" + c.getRazao());
			lore.add("");
			lore.add("�bRecompensa pr�ximo n�vel(�f"+ (level.getLevel() + 1) +"�b): �a" + rewardToString(c.getPremiacao().getReward(level.getLevel()+1)));
			meta.setLore(lore);
			item.setItemMeta(meta);
			inventory.addItem(item);
		}
		if(!isView()) {
			ItemStack item = new ItemStack(Material.BOOK);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName("�6Adquirir novas conquistas.");
			item.setItemMeta(meta);
			inventory.setItem(inventory.getSize()-1, item);
		}
		return inventory;
	}
	public boolean isView() {
		return !player.equalsIgnoreCase(view);
	}
	
	private String rewardToString(Reward[] a) {
		StringBuilder sb = new StringBuilder();
		for(Reward rw : a) {
			sb.append("�2");
			sb.append(rw.getType().getName());
			sb.append(":�f ");
			sb.append(rw.getValor());
			sb.append(" ");
			
		}
		return sb.toString();
	}
	
	public static void main(String[] args) {
	}
	
	
}
