package me.jonasxpx.cubos.inventarios.loja;

import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;

import me.jonasxpx.cubos.Cubos;

public class Loja extends InventoryView{

	
	private final Player player;
	
	public Loja(Player player) {
		this.player = player;
	}
	
	@Override
	public Inventory getBottomInventory() {
		return getPlayer().getInventory();
	}

	@Override
	public HumanEntity getPlayer() {
		return this.player;
	}

	@Override
	public Inventory getTopInventory() {
		Inventory inv = Bukkit.createInventory(getPlayer(), 54, "Loja iCubos");
		Cubos.getLojaItem().forEach(i ->{
			inv.addItem(i.getItem());
		});
		return inv;
	}

	@Override
	public InventoryType getType() {
		return InventoryType.CHEST;
	}
	
	
	
}
