package me.jonasxpx.cubos.inventarios.loja;

import java.util.List;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.jonasxpx.cubos.Cubos;

public class ItemCmd {

	private ItemStack item;
	private int price;
	private List<String> commandOut = null;
	private CommandOut classOut = null;
	
	public ItemCmd(ItemStack item) {
		this.item = item;
	}
	
	public void setLabel(List<String> descricao) {
		ItemMeta meta = getItem().getItemMeta();
		descricao.add("�fValor: �2" + getPrice());
		meta.setLore(descricao);
		getItem().setItemMeta(meta);
	}
	
	public void setName(String name) {
		ItemMeta meta = getItem().getItemMeta();
		meta.setDisplayName(name);
		getItem().setItemMeta(meta);
	}
	
	public ItemStack getItem() {
		return item;
	}
	
	public void setCommandOut(List<String> list) {
		this.commandOut = list;
	}
	
	public List<String> getCommandOut() {
		return commandOut;
	}
	
	public void setClassOut(CommandOut classOut) {
		this.classOut = classOut;
	}
	
	public CommandOut  getClassOut() {
		return classOut;
	}
	
	public int getPrice() {
		return price;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	
	public static ItemCmd getItemByItemStack(ItemStack i) {
		for(ItemCmd ic : Cubos.getLojaItem()) {
			if(ic.getItem().equals(i)) {
				return ic;
			}
		}
		return null;
	}
	
}
