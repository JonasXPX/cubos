package me.jonasxpx.cubos.inventarios;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.jonasxpx.cubos.Cubos;

public enum Itens {

	BALANCO(1, Cubos.itensMensagens.get(1), Material.DIAMOND, null),
	BAUPREMIADO1(2, Cubos.itensMensagens.get(2), Material.CHEST, Cubos.getBauPremiado().getItens(1)),
	COMPRARCONQUISTAS(3, "", Material.BOOK, null);
	
	private int id;
	private String messagem;
	
	private Itens(int id, String nome, Material material, ItemStack[] itens) {
		this.id = id;
	}

	public String getMessagem() {
		return messagem;
	}
	public int getId() {
		return id;
	}
}

