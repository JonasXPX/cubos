package me.jonasxpx.cubos.inventarios;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.jonasxpx.cubos.Cubos;
import me.jonasxpx.cubos.conquistas.Conquista;
import me.jonasxpx.cubos.user.User;

public class Conquistas {

	private final String player;
	private final User user;
	private final boolean isView;
	private String view;
	
	public Conquistas(String player, boolean view) {
		this.player = player;
		this.user = Cubos.getUser(player);
		this.isView = view;
	}
	
	public void openInventory(boolean buy) {
		Inventory inv = Bukkit.createInventory(null, 54, "iCubos | Conquistas");
		if(!isView && buy != true && user.getPlayerConquistas().listenerConquistas.size() == 0) {
			ItemStack item = new ItemStack(Material.BOOK);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName("�cVoc� n�o tem conquistas adiquiridas.");
			meta.setLore(Arrays.asList("�dClique para comprar novas conquistas."));
			item.setItemMeta(meta);
			inv.setItem(inv.getSize() / 4, item);
		}
		if(buy){
			User user = Cubos.getUser(this.user.getUser());
			for(Conquista c : Cubos.getConquistas()) {
				if(user.getPlayerConquistas().hasConquista(c)) {
					continue;
				}
				ItemStack item = new ItemStack(Material.ENCHANTED_BOOK);
				ItemMeta meta = item.getItemMeta();
				meta.setDisplayName("�a" + c.getName());
				meta.setLore(formatString("�bValor: �c R$ " + NumberFormat.getInstance().format(c.getValor()),
						"�dDescri��o: �c", c.getDescricao(),
						"�6Informa��es sobre a conquista:",
						"�bExperiencia a ganhar: �a" + c.getExp(),
						"�bRazao de dificuldade: �a" + c.getRazao()));
				item.setItemMeta(meta);
				inv.addItem(item);
			}
		} else {
			InventarioViewConquitas inventario = new InventarioViewConquitas(player, view);
			inv = inventario.openInventory();
		}
			Bukkit.getPlayer(isView ? view : player).openInventory(inv);
	}
	
	public void setView(String view) {
		this.view = view;
	}
	
	public User getUser() {
		return user;
	}
	
	public List<String> formatString(String ... strings){
		List<String> s = new ArrayList<>();
		for(String st : strings) {
			if(st.length() > 32) {
				String a1 = st.substring(0, st.length() / 2);
				String a2 = st.substring(st.length() / 2);
				s.add(a1);
				s.add(a2);
			} else {
				s.add(st);
			}
		}
		return s;
	}
}
