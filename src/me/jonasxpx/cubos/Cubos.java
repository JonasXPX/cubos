package me.jonasxpx.cubos;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.trc202.CombatTag.CombatTag;
import com.trc202.CombatTagApi.CombatTagApi;

import me.jonasxpx.cubos.conquistas.BauPremiado;
import me.jonasxpx.cubos.conquistas.Conquista;
import me.jonasxpx.cubos.database.DataBase;
import me.jonasxpx.cubos.inventarios.loja.ItemCmd;
import me.jonasxpx.cubos.listeners.PlayerListeners;
import me.jonasxpx.cubos.tools.ItemConfigRead;
import me.jonasxpx.cubos.tools.Premios;
import me.jonasxpx.cubos.tools.PremiosTools;
import me.jonasxpx.cubos.user.Commands;
import me.jonasxpx.cubos.user.Notificacao;
import me.jonasxpx.cubos.user.Transferencia;
import me.jonasxpx.cubos.user.User;
import net.milkbowl.vault.economy.Economy;

public class Cubos extends JavaPlugin{

	private static List<User> usersList = new ArrayList<>();
	private static Map<String, Transferencia> transferencias = new HashMap<>();
	public static Map<Integer, String> itensMensagens = new HashMap<>();
	private static DataBase db;
	private static CombatTagApi ct;
	private static BauPremiado bauPremiado;
	private static List<Conquista> conquistas;
	private static List<ItemCmd> lojaItem;
	public static final String TAG = "�7[�2iCubos�7]";
	private static Cubos cubosInstance;
	private static Economy eco;
	
	@Override
	public void onEnable() {
		setupEconomy();
		getConfig().options().copyDefaults(true);
		saveDefaultConfig();
		String userDb = getConfig().getString("mysql.user");
		String passDb = getConfig().getString("mysql.password");
		cubosInstance = this;
		StringBuilder sb = new StringBuilder();
		sb.append(getConfig().getString("mysql.ip"));
		sb.append(":");
		sb.append(getConfig().getInt("mysql.port"));
		sb.append("/");
		sb.append(getConfig().getString("mysql.database"));
		
		db = new DataBase(sb.toString(), userDb, passDb);
		if(!db.connect()) {
			this.getPluginLoader().disablePlugin(this);
			return;
		}
		ct = new CombatTagApi((CombatTag) getServer().getPluginManager().getPlugin("CombatTag"));
		getCommand("icubos").setExecutor(new Commands());
		getServer().getPluginManager().registerEvents(new PlayerListeners(), this);
		loadConquistas();
		loadItem();
		getUsersList().clear();
		for(Player p : getServer().getOnlinePlayers()) {
			User u = new User(p.getName());
			u.load();
			getUsersList().add(u);
		}
		Notificacao.noConquistaNotification();
	}
	
	
	private void loadItem() {
		lojaItem = new ArrayList<>();
		for(String key : getConfig().getConfigurationSection("itens").getKeys(false)) {
			ItemConfigRead icr = new ItemConfigRead(key, getConfig());
			ItemStack is = new ItemStack(icr.getItemId());
			ItemCmd i = new ItemCmd(is);
			try {
				if(icr.getClassOut() != null)
					i.setClassOut(icr.getClassOut());
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			i.setCommandOut(icr.getCommandOut());
			i.setPrice(icr.getPrice());
			i.setLabel(icr.label());
			i.setName(icr.name());
			lojaItem.add(i);
		}
	}


	public static List<User> getUsersList() {
		return usersList;
	}
	
	public static DataBase getDataBase() {
		return db;
	}
	public static List<ItemCmd> getLojaItem() {
		return lojaItem;
	}

	public static Cubos getCubosInstance() {
		return cubosInstance;
	}
	
	public static User getUser(String name) {
		for (User user : usersList) {
			if(user.getUser().equalsIgnoreCase(name))
				return user;
		}
		User tmp = new User(name);
		tmp.load();
		usersList.add(tmp);
		return tmp;
	}
	
	public static CombatTagApi getCombatTag() {
		return ct;
	}
	
	public static Map<String, Transferencia> getTransferencias() {
		return transferencias;
	}
	
	@Override
	public void onDisable() {
		if(db != null) {
			for(User user : getUsersList()) {
				user.saveData();
			}
		}
	}
	
	public static BauPremiado getBauPremiado() {
		return bauPremiado;
	}
	
	public static List<Conquista> getConquistas() {
		return conquistas;
	}
	
	public static Conquista getConquistasByName(String name) {
		for(Conquista cq : conquistas) {
			if(cq.getName().equalsIgnoreCase(name)) {
				return cq;
			}
		}
		return null;
	}
	
	public static Economy getEconomy() {
		return eco;
	}
	
	private void loadConquistas() {
		conquistas = new ArrayList<>();
		Set<String> conq = getConfig().getConfigurationSection("conquistas").getKeys(false);
		
		for(String s : conq) {
			String name = s;
			int id = getConfig().getInt("conquistas." + s + ".id");
			double razao = getConfig().getDouble("conquistas." + s + ".razao");
			double experiencia = getConfig().getDouble("conquistas." + s + ".experiencia");
			double valor = getConfig().getDouble("conquistas." + s + ".valor");
			String descricao = ChatColor.translateAlternateColorCodes('&', getConfig().getString("conquistas." + s + ".descricao"));
			
			Set<String> p = getConfig().getConfigurationSection("conquistas." + s + ".premios").getKeys(false);
			JSONObject premios = new JSONObject();
			for (String string : p) {
				JSONObject j;
				try {
					j = new JSONObject(getConfig().getString("conquistas." + s + ".premios." + string));
					premios.put(string, j);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			Premios premio = PremiosTools.executePremios(premios);
			Conquista c = new Conquista(name, razao, id, premio);
			c.setExp(experiencia);
			c.setValor(valor);
			c.setDescricao(descricao);
			c.setValor(valor);
			conquistas.add(c);
		}
	}
	
	private boolean setupEconomy()
    {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            eco = economyProvider.getProvider();
        }

        return (eco != null);
    }
	
	public static void updatePairs() {
		try {
			URL url = new URL("https://forex.1forge.com/1.0.3/quotes?pairs=USDEUR&api_key=Qf9v5s7jiybSt3fApZBGQm7ARCneL9y2");
			URLConnection c = url.openConnection();
			InputStream in = c.getInputStream();
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int length;
			while((length = in.read(buffer)) != -1) {
				bout.write(buffer, 0, length);
			}
			JSONArray jsonArray = new JSONArray(bout.toString(StandardCharsets.UTF_8.name()));
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void log(Level level, String msg) {
		getCubosInstance().getLogger().log(level, msg);
	}
	
	public static double getCurrencySell(String g) {
		return 1.5123;
	}
	
	public static double getCurrencyBuy(String g) {
		return 1.5124;
	}
}
