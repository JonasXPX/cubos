package me.jonasxpx.cubos.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONObject;


public class DataBase {

	private Connection connection;
	private final String url, user, password;
	
	public DataBase(String url, String user, String password) {
		this.url = url;
		this.user = user;
		this.password = password;
	}
	
	public JSONObject loadUserData(String player) {
		try {
			PreparedStatement ps = connection.prepareStatement("SELECT data FROM iCubos WHERE username = ?");
			ps.setString(1, player);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				return new JSONObject(rs.getString(1));
			}
			else
				return null;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public int createUserData(String player) {
		try {
			PreparedStatement ps = connection.prepareStatement("INSERT INTO iCubos VALUES(?, 0, \"{}\")");
			ps.setString(1, player);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	public void saveUserData(String player, JSONObject data) {
		try {
			PreparedStatement ps = connection.prepareStatement("UPDATE iCubos SET data = ? WHERE username = ?");
			ps.setString(1, data.toString());
			ps.setString(2, player);
			ps.executeUpdate();
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public boolean connect() {
		boolean con;
		try {
			DriverManager.setLoginTimeout(0);
			this.connection = DriverManager.getConnection("jdbc:mysql://" + url, user, password);
			connection.prepareStatement("CREATE TABLE IF NOT EXISTS iCubos(username VARCHAR(32), cubos INT(32), data TEXT)").execute();
			con = true;
		} catch (SQLException e) {
			con = false;
			e.printStackTrace();
		}
		return con;
	}
	
	public void stopConnection() {
			try {
				if(this.connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}
	
}
