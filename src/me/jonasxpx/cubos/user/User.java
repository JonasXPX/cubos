package me.jonasxpx.cubos.user;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.json.JSONException;
import org.json.JSONObject;

import me.jonasxpx.cubos.Cubos;
import me.jonasxpx.cubos.user.carteira.Carteira;

public class User{

	private final String user;
	private JSONObject data;
	private PlayerConquista playerConquistas;
	private Carteira carteira;
	
	public User(String user) {
		this.user = user;
	}
	
	public void load() {
		this.data = Cubos.getDataBase().loadUserData(user);
		if(data == null) {
			Cubos.getDataBase().createUserData(user);
			this.data = Cubos.getDataBase().loadUserData(user);
			try {
				data.put("cubos", 0);
				Map<Object, Object> map = new HashMap<>();
				map.put(System.currentTimeMillis(), 1);
				data.put("investido", map);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		playerConquistas = new PlayerConquista(getUser(), this);
		playerConquistas.loadConquistas();
		
		//carteira = new Carteira(getData(), this);
	}
	
	
	public String getUser() {
		return user;
	}
	
	public JSONObject getData() {
		return data;
	}
	
	public void updateDate(JSONObject data) {
		this.data = data;
	}
	
	public void saveData() {
		Cubos.getCubosInstance().getLogger().log(Level.INFO, "Data saved");
		getPlayerConquistas().saveConquistas();
		Cubos.getDataBase().saveUserData(user, getData());
	}
	
	public void addInvestimento(int cubos) {
		Map<Object, Object> map;
		try {
			map = (Map<Object, Object>) data.get("investido");
			map.put(System.currentTimeMillis(), cubos);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	public String verInvestimento() {
		return "0 �d(�6 http://bit.ly/Carteira-iCubos �d)";
	}
	
	
	public Player getPlayer() {
		return Bukkit.getPlayer(user);
	}
	
	public PlayerConquista getPlayerConquistas() {
		return playerConquistas;
	}
	
	
}
