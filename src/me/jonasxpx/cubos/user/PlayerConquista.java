package me.jonasxpx.cubos.user;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.bukkit.Bukkit;
import org.json.JSONException;
import org.json.JSONObject;

import me.jonasxpx.cubos.Cubos;
import me.jonasxpx.cubos.conquistas.Conquista;
import me.jonasxpx.cubos.conquistas.Level;

public class PlayerConquista {

	public final String player;
	public HashMap<Conquista, Level> listenerConquistas;
	public User user;
	
	
	public PlayerConquista(String player, User user) {
		this.player = player;
		this.user = user;
	}

	
	public void loadConquistas() {
		listenerConquistas = new HashMap<>();
		if(!user.getData().has("conquistas")) {
			return;
		}
		try {
			JSONObject j = user.getData().getJSONObject("conquistas");
			Iterator<String> keys = j.keys();
			keys.forEachRemaining(p -> {
				JSONObject conq = null;
				try {
					conq = j.getJSONObject(p);
				} catch (JSONException e) {e.printStackTrace();}
				for(Conquista c : Cubos.getConquistas()) {
					if(c.getId() == Integer.parseInt(p)) {
						try {
							listenerConquistas.put(c, new Level(c, conq.getInt("level"), conq.getInt("experiencia"), Bukkit.getPlayer(player)));
						} catch (JSONException e) {e.printStackTrace();}
					}
				}
			});
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void saveConquistas() {
		Map<String, Map<String, Double>> s = new HashMap<>();
		for (Conquista c : listenerConquistas.keySet()) {
			Map<String, Double> ss = new HashMap<>();
			Level l = listenerConquistas.get(c);
			ss.put("level", (double)l.getLevel());
			ss.put("experiencia", l.getExperience());
			s.put(Integer.toString(c.getId()), ss);
		}
		try {
			user.getData().put("conquistas", s);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void addConquista(Conquista c) {
		
		Map<String, Map<String, Integer>> s = new HashMap<>();
		Map<String, Integer> ss = new HashMap<>();
		ss.put("level", 0);
		ss.put("experiencia", 0);
		s.put(Integer.toString(c.getId()), ss);
		try {
			user.getData().put("conquistas", s);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		listenerConquistas.put(c, new Level(c, 0, 0, Bukkit.getPlayer(player)));
	}
	
	public boolean hasConquista(Conquista cq) {
		return listenerConquistas.containsKey(cq);
	}
	public Conquista getConquista(String name) {
		for(Conquista c : listenerConquistas.keySet()) {
			if(c.getName().equals(name))
				return c;
		}
		return null;
	}
	
}
