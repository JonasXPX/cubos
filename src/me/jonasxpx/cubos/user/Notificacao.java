package me.jonasxpx.cubos.user;

import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import me.jonasxpx.cubos.Cubos;

public class Notificacao {

	
	public static BukkitTask noConquistaNotification() {
		BukkitTask task;
		
		task = new BukkitRunnable() {
			
			@Override
			public void run() {
				for (User users : Cubos.getUsersList()) {
					if(users.getPlayerConquistas().listenerConquistas.size() == 0) { 
						users.getPlayer().sendMessage("�7[�2iCubos�7] �bAtive suas conquistas! �o/icubos conquistas");
					}
				}
			}
		}.runTaskTimer(Cubos.getCubosInstance(), 20 * 120, 20 * 240);
		
		return task;
	}
}
