package me.jonasxpx.cubos.user;

import static me.jonasxpx.cubos.Cubos.getCurrencyBuy;
import static me.jonasxpx.cubos.Cubos.getCurrencySell;

public enum Types {

	USDEUR("USDEUR", getCurrencyBuy("USDEUR"), getCurrencySell("USDEUR"));
	
	
	private String name;
	private float currencyBuy;
	private float currencySell;
	
	private Types(String name, double currencyBuy, double currencySell) {
		this.name = name;
		
	}
	
	public String getName() {
		return name;
	}
	
	public double[] getCurrencys() {
		return new double[] {currencyBuy, currencySell};
	}
	
	public static Types fromString(String string) {
		for (Types t : Types.values()) {
			return t;
		}
		return null;
	}
	
}
