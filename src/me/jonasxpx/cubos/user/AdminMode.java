package me.jonasxpx.cubos.user;

import java.util.logging.Level;

import org.json.JSONException;
import org.json.JSONObject;

import me.jonasxpx.cubos.Cubos;

public class AdminMode {
	
	
	public static void giveICubos(User receiver, int toGive) {
		try {
			JSONObject dataReceiver = receiver.getData();
			int cubos = dataReceiver.getInt("cubos");
			dataReceiver.put("cubos", cubos+=toGive);
			receiver.updateDate(dataReceiver);
		}catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public static void setICubos(User receiver, int toSet) {
		try {
			
			JSONObject dataReceiver = receiver.getData();
			dataReceiver.put("cubos", toSet);
			receiver.updateDate(dataReceiver);
			
		}catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @throws IllegalArgumentException
	 */
	public static void takeICubos(User receiver, int toTake) {
		try {
			Cubos.getCubosInstance().getLogger().log(Level.INFO, "debug1");
			JSONObject dataReceiver = receiver.getData();
			int cubos = dataReceiver.getInt("cubos");
			if(toTake > cubos) {
				Cubos.getCubosInstance().getLogger().log(Level.INFO, "debug2");
				throw new IllegalArgumentException("jogador sem cubos suficientes");
			}
			Cubos.getCubosInstance().getLogger().log(Level.INFO, "iCubos to taken: " + toTake);
			dataReceiver.put("cubos", cubos-=toTake);
			receiver.updateDate(dataReceiver);

			Cubos.getCubosInstance().getLogger().log(Level.INFO, "debug3");
			
		}catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public static int getTotalCubos(User receiver) {
		try {
			return receiver.getData().getInt("cubos");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return -1;
	}
}
