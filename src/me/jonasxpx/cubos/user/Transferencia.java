package me.jonasxpx.cubos.user;

import org.json.JSONException;
import org.json.JSONObject;

import me.jonasxpx.cubos.Cubos;

public class Transferencia {

	protected User sender;
	protected User receiver;
	protected int cubos;
	private int cubosSender;
	private int cubosReceiver;
	private JSONObject dataSender;
	private JSONObject dataReceiver;
	
	public Transferencia(User sender, User receiver, int cubos) {
		this.sender = sender;
		this.receiver = receiver;
		
		dataSender = sender.getData();
		dataReceiver = receiver.getData();
		
		try {
			cubosSender = dataSender.getInt("cubos");
			cubosReceiver = dataReceiver.getInt("cubos");
			if(cubos > cubosSender) {
				finalizarErro("�2Transa��o finalazada por falta de Cubos", null);
				return;
			}
			if(sender.equals(receiver)) {
				finalizarErro("�2Transa��o n�o pode ser feita entre si.", null);
				return;
			}
			if(Cubos.getTransferencias().containsKey(sender.getUser())) {
				Transferencia t = Cubos.getTransferencias().get(sender.getUser());
				finalizarErro("�2Voc� deve finalizar a trasa��o atual de �f" + t.cubos + " Cubo(s) �2para �f" + t.receiver.getUser(), null);
				return;
			}
			
			this.cubos = cubos;
			
			cubosSender-=cubos;
			
			dataSender.put("cubos", cubosSender);
			
			finalizarErro("�2Voc� deve confirmar a transferencia antes.\n"
					+ "Enviar �f" + cubos + " Cubo(s) �2para �f" + receiver.getUser() + "�2 ?\n"
							+ "�bDigite �2sim �bou �cnao �bno chat.",
					"�2Aguarde o jogador confirmar a transferencia de �f"+ cubos +" Cubo(s) �2para sua conta.");
			
			Cubos.getTransferencias().put(sender.getUser(), this);
		}catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	
	public void confirmar() {
		try {
			
			cubosReceiver+=cubos;
			
			dataReceiver.put("cubos", cubosReceiver);

			finalizarErro("�2Transa��o efetuada com sucesso", "�2Voc� recebeu �f" + cubos + " Cubo(s) �2de �f" + sender.getUser());
			
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			Cubos.getTransferencias().remove(sender.getUser());
		}
	
	}
	
	public void cancelar() {
		try {
			
			cubosSender+=cubos;
			dataSender.put("cubos", cubosSender);
			
			finalizarErro("�2Transa��o cancelada.", "�2Transa��o n�o foi autorizada pelo jogador.");
			
		}catch (JSONException e) {
			e.printStackTrace();
		} finally {
			Cubos.getTransferencias().remove(sender.getUser());
		}
	}
	
	public void naoEfetuada() {
		try {
			
			cubosSender+=cubos;
			dataSender.put("cubos", cubosSender);
			
			finalizarErro("�2Transa��o n�o efetuada.", "�2Transa��o n�o foi efetuada pelo jogador.");
			
		}catch (JSONException e) {
			e.printStackTrace();
		} finally {
			Cubos.getTransferencias().remove(sender.getUser());
		}
	}
	
	
	public void finalizarErro(String erroSender, String erroReceiver) {
		if(erroSender != null)
			sender.getPlayer().sendMessage("\u2022 "+ erroSender);
		if(erroReceiver != null)
			if(receiver.getPlayer()!=null)
				receiver.getPlayer().sendMessage("\u2022 "+erroReceiver);
	}
	
	
	
	
}
