package me.jonasxpx.cubos.user;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.json.JSONException;

import me.jonasxpx.cubos.Cubos;
import me.jonasxpx.cubos.inventarios.Conquistas;
import me.jonasxpx.cubos.inventarios.loja.Loja;

public class Commands implements CommandExecutor{

	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(args.length >= 1) {
			try {
				if(args[0].equalsIgnoreCase("pay")) {
					if(args.length <= 2) {
						sender.sendMessage("�cUso correto /" + label + " pay <usuario> <quantia>");
						return true;
					}
					new Transferencia(Cubos.getUser(sender.getName()), Cubos.getUser(args[1]), Integer.parseInt(args[2]));
					return true;
				}
				if(args[0].equalsIgnoreCase("conquistas")) {
					Conquistas c;
					if(args.length >= 2) {
						if(!Bukkit.getOfflinePlayer(args[1]).hasPlayedBefore()) {
							sender.sendMessage("�cJogador n�o encontrado.");
							return true;
						}
						c = new Conquistas(args[1], true);
						c.setView(((Player)sender).getName());
						if(c.getUser().getPlayerConquistas().listenerConquistas.size() == 0) {
							sender.sendMessage("�cJogador n�o tem conquistas ativas para mostrar.");
							return true;
						}
					} else {
						c = new Conquistas(((Player)sender).getName(), false);
						c.setView(sender.getName());
					}
					c.openInventory(false);
					return true;
				}
				if(args[0].equalsIgnoreCase("investir")) {
					sender.sendMessage("�cComando n�o dispon�vel no momento.");
					return true;
				}
				if(args[0].equalsIgnoreCase("loja")) {
					if(!sender.hasPermission("icubos.loja")) {
						sender.sendMessage(Cubos.TAG + "�c No momento voc� n�o pode usar a loja.");
						return true;
					}
					Loja loja = new Loja((Player)sender);
					((Player)sender).openInventory(loja);
					return true;
				}
				if(args[0].equalsIgnoreCase("help")) {
					sender.sendMessage("�dAjuda de Comandos�c\n"
							+ "Acesso a conquistas: /"+label+" conquistas\n"
							+ "Pagamentos: /" + label + " pay <jogador> <quantia>\n"
									+ "Investimento: /"+ label + " investir");
					return true;
				}
				if(sender.isOp() || sender.hasPermission("cubos.admin")) {
					if(args[0].equalsIgnoreCase("give")) {
						AdminMode.giveICubos(Cubos.getUser(args[1]), Integer.parseInt(args[2]));
						return true;
					}
					if(args[0].equalsIgnoreCase("take")) {
						AdminMode.takeICubos(Cubos.getUser(args[1]), Integer.parseInt(args[2]));
						return true;
					}
					if(args[0].equalsIgnoreCase("set")) {
						AdminMode.setICubos(Cubos.getUser(args[1]), Integer.parseInt(args[2]));
						sender.sendMessage("�ciCubos de jogador alterado.");
						return true;
					}
				}
			}catch (NumberFormatException e) {
				sender.sendMessage("�cFormato incorreto para o valor informado.");
				return true;
			}
			if(sender.hasPermission("cubos.checkoffline")) {
				String player = args[0];
				if(!Bukkit.getOfflinePlayer(player).hasPlayedBefore()) {
					sender.sendMessage("�cJogador n�o encontrado.");
					return true;
				}
				try {
					User user = Cubos.getUser(player);
					sender.sendMessage("�2Balan�o de "+ user.getUser() +": �f" + user.getData().getInt("cubos"));
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return true;
			}
		}
		if(args.length == 0) {
			if(!sender.hasPermission("cubos.checkbalance")) {
				sender.sendMessage("Sem permi��o");
				return true;
			}
			User user = Cubos.getUser(sender.getName());
			try {
				sender.sendMessage("�6Outros comandos: �f/icubos loja /icubos invetir\n"
						+ "�2Balan�o: �f" + user.getData().getInt("cubos") + " Cubos\n" + 
									"�2Investido: �f" + user.verInvestimento());
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
	
}
