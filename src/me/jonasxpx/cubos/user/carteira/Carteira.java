package me.jonasxpx.cubos.user.carteira;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import me.jonasxpx.cubos.user.Types;
import me.jonasxpx.cubos.user.User;

public class Carteira {

	
	private List<Investimento> investimentos;
	
	public Carteira(JSONObject data, User player) {
		investimentos = new ArrayList<>();
		try {
			JSONObject carteira = data.getJSONObject("carteira");
			for(Types t : Types.values()) {
				if(carteira.has(t.getName())) {
					JSONObject i = carteira.getJSONObject(t.getName());
					Investimento inv = new Investimento();
					inv.setQuantia(i.getInt("quantia"));
					inv.setPair(Types.fromString(i.getString("pair")));
					inv.setValorDeCompra(i.getDouble("aplicado"));
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void variarInvestimentos(Types type, double variacao) {
		for(Investimento i : investimentos) {
			if(i.getPair().equals(type))
				i.setVariavel(variacao);
		}
	}
	
	
	public void fecharInvestimento(Types type) {
		
	}
	
	public void aplicar(int quantia, Types types) {
		Investimento i = new Investimento();
		i.setQuantia(quantia);
		i.setPair(types);
		i.setValorDeCompra(types.getCurrencys()[0]);
		
	}
	
	
	public static void main(String[] args) {
		Investimento i = new Investimento();
		i.setPair(Types.USDEUR);
		i.setQuantia(2);
		i.setValorDeCompra(2.5525);
		System.out.println(i.getVariavel());
	}
}
