package me.jonasxpx.cubos.user.carteira;

import me.jonasxpx.cubos.user.Types;

public class Investimento {
	
	private Types pair;
	private double aplicado;
	private int quantia = 0;
	private double variavel = 0;
	
	public Investimento() {
	}

	public double getVariavel() {
		System.out.println(getPair().getCurrencys()[0]);
		return (getPair().getCurrencys()[1] - getPair().getCurrencys()[0]) / getPair().getCurrencys()[0] * 100;
	}

	public void setVariavel(double variavel) {
		this.variavel = variavel;
	}
	
	public void setQuantia(int quantia) {
		this.quantia = quantia;
	}
	
	public int getQuantia() {
		return quantia;
	}
	
	public void setPair(Types pairs) {
		this.pair = pairs;
	}
	
	public Types getPair() {
		return pair;
	}
	
	public void setValorDeCompra(double aplicado) {
		this.aplicado = aplicado;
	}
	
	public double getValorDeCompra() {
		return aplicado;
	}
	
}
